import sys
import csv

from principal import *

import luigi

#luigid -debug luigi
#python3 luigi_run.py DBData --local-scheduler

# Mapea departamento
class Mapeo(luigi.Task):
    n = luigi.IntParameter()
    def requires(self):
        return []

    def output(self):
        return luigi.local_target('Incautacion_T1.csv')

    def run(self):
        salida = []
        with self.output().open('r') as f:
            for line in f:
                line = line.strip()
                campo = line.split(',')
                if campo[0] not in salida:
                    # si existe la llave se agrega el valor
                    salida.append(campo[0])
                

# Almacena departamento
class DBData(luigi.Task):
    def requires(self):
        return []

    def output(self):
        return luigi.LocalTarget('dptos.txt')

    def run(self):
        Init_DB()
        print("Hi")
        origen = 'Incautacion_T1.csv'
        salida = []
        municipio = []
        ignorar = True
        with open(origen) as f_in, self.output().open('w') as f_out:
            limit = 0
            for linea in f_in:
                linea = linea.strip()
                campo = linea.split(',')
                if ignorar:
                    ignorar=False
                else:
                    if campo[0] not in salida:
                        #si existe la llave agrega el valor
                        salida.append(campo[0])
                        f_out.write('{}\n'.format(campo[0]))
                        Add_Dpto(campo[0])

                    if campo[1] not in municipio:
                        Add_Municipio(campo[1], campo[2], campo[0])
                        

                    campo[5] = campo[5].replace(".", "") # quitar los puntos de los float

                    Add_Incautacion(campo[4], campo[3], campo[5], campo[2])
                

if __name__ == '__main__':
    luigi.run()