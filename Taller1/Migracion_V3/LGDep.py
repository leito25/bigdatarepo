import luigi

#luigid -debug luigi
#python3 LGDep.py Departamento --local-scheduler

# Mapea departamento
class Mapeo(luigi.Task):
    n = luigi.IntParameter()
    def requires(self):
        return []

    def output(self):
        return luigi.local_target('Incautacion_T1.csv')

    def run(self):
        salida = []
        with self.output().open('r') as f:
            for line in f:
                line = line.strip()
                campo = line.split(',')
                if campo[0] not in salida:
                    # si existe la llave se agrega el valor
                    salida.append(campo[0])

# Almacena departamento
class Departamento(luigi.Task):
    def requires(self):
        return []

    def output(self):
        return luigi.LocalTarget('dptos.txt')

    def run(self):
        origen = 'Incautacion_T1.csv'
        salida = []
        with open(origen) as f_in, self.output().open('w') as f_out:
            for linea in f_in:
                linea = linea.strip()
                campo = linea.split(',')
                if campo[0] not in salida:
                    #si existe la llave agrega el valor
                    salida.append(campo[0])
                    f_out.write('{}\n'.format(campo[0]))

if __name__ == '__main__':
    luigi.run()