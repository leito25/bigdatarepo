
from sqlalchemy import create_engine
from sqlalchemy.orm import session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

#https://docs.sqlalchemy.org/en/14/dialects/mysql.html#module-sqlalchemy.dialects.mysql.pymysql
# nombre de la database = mybigdatabase
motor = create_engine('mysql+pymysql://root:leito123@localhost/mybigdatabase')

Session = sessionmaker(bind=motor)
session = Session()
Base = declarative_base()