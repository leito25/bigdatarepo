# alembic - alchemy
from sqlalchemy.sql.expression import null
import db
from modelost import *

def Add_Dpto(dep):
    reg_dpto = Departamento(departamento=dep)
    db.session.merge(reg_dpto) # utilizo el merge para evitar repeticiones
    db.session.commit()

def Add_Municipio(mun, cdane, dpto):
    reg_mun = Municipio(id=cdane, municipio=mun, depto=dpto)
    db.session.merge(reg_mun)
    db.session.commit()

def Add_Incautacion(fecha, claseb, cant, cdane):
    reg_inca = Incautacion(fechainc=fecha, clasebien=claseb, cantidad=cant, codigodane=cdane)
    db.session.add(reg_inca)
    db.session.commit()

# Agregue este método para limpiar las tablas y volverlas a crear
# al momento de vaciar el .cvs en la database
def Init_DB():
    db.Base.metadata.drop_all(db.motor)
    db.Base.metadata.create_all(db.motor)

# Esto solo se ejecuta cuando se corre principal.py
if __name__ == '__main__':
    db.Base.metadata.drop_all()
    db.Base.metadata.create_all(db.motor)