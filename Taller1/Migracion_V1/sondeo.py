import sys
import csv

from principal import *

'''import db
from modelos import *
from principal import *'''

#python3 script.py csv-file n-data

# linea de ejecución
#python3 sondeo.py Incautacion_T1.csv 10



if __name__ == '__main__':
    arg = sys.argv
    if len(arg) == 3:
        print('nombre del archivo: ', arg[1])
        print('limite de datos: ', arg[2])
        # print('Cantidad de argumentos correctos')

        nombre_archivo = arg[1]
        limite = int(arg[2])

        archivo = open(nombre_archivo, 'r')
        lista_datos = {}
        cont = 0
        ignorar = True
        # init database
        # Este método es para limpiar las tablas
        # las borra y las crea nuevamente
        Init_DB()

        for linea in archivo:
            linea = linea.strip()
            campos = linea.split(',')
            if ignorar:
                ignorar=False
            else:
                Add_Dpto(campos[0])
                Add_Municipio(campos[1], campos[2], campos[0])
                Add_Incautacion(campos[4], campos[3], campos[5], campos[2])
                print(campos[0], campos[1], campos[2], campos[3], campos[4], campos[5])
            if cont > limite:
                break
            cont+=1


    else:
        print('Argumentos incorrectos')
