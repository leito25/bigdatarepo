from sqlalchemy.sql.expression import column, true
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.sql.sqltypes import Float, String
import db
from sqlalchemy import Column, Integer, Text, Boolean

#['DEPARTAMENTO', 'MUNICIPIO', 'CODIGO DANE', 'CLASE BIEN', 'FECHA HECHO', 'CANTIDAD']

class Departamento(db.Base):
    __tablename__='departamento'
    #id=Column(Integer, autoincrement=True, primary_key = True)
    #id=Column(Integer, autoincrement=True, index=True)
    # Deje el nombre del departamento como primary key
    departamento=Column('ndepartamento', String(100), primary_key=True)

class Municipio(db.Base):
    __tablename__='municipio'
    id=Column('codigodane', String(20), primary_key = True)
    municipio=Column('municipio', Text)
    #id_dep = Column('id_dep', Integer, ForeignKey(Departamento.id))
    #cambié el foreing del id al nombre y funciona bien
    depto = Column('ndepartamento', String(100), ForeignKey(Departamento.departamento))

class Incautacion(db.Base):
    __tablename__ = 'incautacion'
    id = Column(Integer, primary_key = True)
    fechainc=Column('fecha', Text)
    clasebien=Column('clasebien', Text)
    cantidad=Column('cantidad', Float)
    codigodane=Column('codigodane', String(20), ForeignKey(Municipio.id))