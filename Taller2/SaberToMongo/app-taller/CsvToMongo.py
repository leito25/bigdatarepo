import sys
import csv
import os
from pymongo import * 
from sqlalchemy import inspect


import app

ip = 'localhost'
puerto = 27017
nom_db = 'Icfes'
cliente = MongoClient(ip, puerto)
db = cliente[nom_db]


import luigi

# python3 luigiExtractFromSQLDB.py DBMongoCreation --local-scheduler
                

# Se toma el número de registros en SQL+1
# y se hace de una en una las querys y se copian al csv.
class DBExtract(luigi.Task):
    def requires(self):
        return []

    def output(self):
        #sqlDB.csv cambiar por sqlDB_test.csv for testing
        return luigi.LocalTarget('SaberDB.csv')

    def run(self):
        return []

# AMAZONAS,LETICIA (CT),91001000,COCAINA,1/01/2010,8825
# DEPARTAMENTO,MUNICIPIO,CODIGO DANE,CLASE BIEN,FECHA HECHO,CANTIDAD
class DBMongoCreation(luigi.Task):
    def requires(self):
        return [DBExtract()]

    def output(self):
        return []

    def run(self):
        # TODO insertar los registros en mongo
        os.system('mongoimport --db Icfes --collection SaberDB --type=csv --file SaberDB.csv --headerline')





if __name__ == '__main__':
    luigi.run()