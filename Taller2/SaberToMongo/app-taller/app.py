from jinja2.loaders import ModuleLoader
import sqlalchemy
from sqlalchemy.sql.expression import text
from werkzeug.utils import import_string


import flask
from flask import Flask, jsonify, request
from flask_pymongo import PyMongo
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql.sqltypes import Float, String, Text, Integer
from sqlalchemy.sql.schema import Column, ForeignKey
from sqlalchemy import func, sql


import sys
import csv
import os

from pymongo import * 

# APP CREATION
app=Flask(__name__)
# db original
# app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:leito123@localhost/SaberDB'
# db original + promedio
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:leito123@localhost/saberdb'
db = SQLAlchemy(app)



# MODELO
class Departamento(db.Model):
    __tablename__='departamento'
    id=db.Column('dpto_cod', Integer, primary_key=True)
    departamento=db.Column('dpto_nombre', Text)
    glob_prom=db.Column('dpto_globalprom', Integer)
    
class Colegio(db.Model):
    __tablename__='colegio'
    id=db.Column('codigodane', String(20), primary_key = True)
    dpto_cod = db.Column('dpto_cod', Integer, ForeignKey(Departamento.id))
    dpto_nombre = db.Column('dpto_nombre', Text)
    cole_mun_cod = db.Column('cole_mun_cod', Text)
    cole_mun_nombre = db.Column('cole_mun_nombre', Text)
    cole_naturaleza = db.Column('cole_naturaleza', Text)
    cole_area = db.Column('cole_area', Text)

class Estudiante(db.Model):
    __tablename__ = 'estudiante'
    id = db.Column('id', String(20), primary_key = True)
    genero=db.Column('genero', String(1))
    puntaje_global=db.Column('puntaje_global', Integer)
    dpto_cod = db.Column('dpto_cod', Integer, ForeignKey(Colegio.dpto_cod))
    dpto_nombre = db.Column('dpto_nombre', Text)




# SQL Init
@app.route("/")
def home():
    #db.create_all()
    return "<p> <a href='http://127.0.0.1:5000/promedio'>enlace a promedio</a></br> <a href='http://127.0.0.1:5000/promedio_alchemy'>enlace a promedio alchemy</a></p>"

@app.route("/promedio")
def promedio():
    if 'dpto' in request.args:
        nom_dpto = request.args['dpto']
    else:
        return 'Dpto no encontrado'
        
    
    # numero de registros - antioquia
    prom = db.session.query(Estudiante.puntaje_global).\
        join(Colegio, Colegio.dpto_cod == Estudiante.dpto_cod).\
            join(Departamento, Departamento.departamento == nom_dpto).count()

    
    mysql_query_ptext = "SELECT avg(puntaje_global) FROM estudiante INNER JOIN colegio ON colegio.dpto_cod = estudiante.dpto_cod INNER JOIN departamento ON departamento.dpto_cod = colegio.dpto_cod WHERE departamento.dpto_nombre = '%s';"
    mysql_query = sqlalchemy.text(mysql_query_ptext % (nom_dpto))
    ress = db.session.execute(mysql_query)
    result_as_list = ress.fetchall()

    print("SQL Query directa: ")
    resultQuery = ""
    for row in result_as_list:
        print(row)
        resultQuery = str(row)
    resultQuery = resultQuery.replace("(","").replace(",)","")


    print(type(prom))

    myquery = db.session.query(Estudiante.puntaje_global).count()
    #cantidad de registros - 15435
    print("total registros estudiante 15435")
    print(myquery)

    #cantidad de registros de antioquia - 15435
    print("total registros antioquia 19305")
    print(prom)
    resultQuery = resultQuery.replace("Decimal'", "").replace("')","")
    html_line = "<h1> Dpto: "+ nom_dpto + " Prom: " + resultQuery + "</h1>"
    return html_line

@app.route("/promedio_alchemy")
def promedio_alch():
    if 'dpto' in request.args:
        nom_dpto = request.args['dpto']
    else:
        return 'Dpto no encontrado'
    
    # numero de registros - antioquia
    prom = db.session.query(func.avg(Estudiante.puntaje_global)).\
        join(Colegio, Colegio.dpto_cod == Estudiante.dpto_cod).\
            join(Departamento, Departamento.departamento == nom_dpto)

    promValue = ""
    for data in prom:
        print("DATA data")
        promValue = str(data)
        print(promValue)
    promValue = promValue.replace("(Decimal('", "").replace("'),)","")
    html_line = "<h1>Alch.  Dpto: "+ nom_dpto + " Prom: " + promValue + "</h1>"
    return html_line


    # SQL Init
@app.route("/test")
def test():
    #insercion = {'DEPARTAMENTO':cad[0], 'MUNICIPIO':cad[1], 'CODIGO_DEPARTAMENTO':cad[2], 'CLASE_BIEN':cad[3], 'FECHA':cad[4], 'CANTIDAD':int(intCantidad)}
    #registros = db.Incautaciones_test.insert([insercion])
    reg = ['M', 123, '123', 'AMAX']
    
    #Add_Depto(1,'AMAX') ok
    # cod-dane, dptocod, dptoname, cole-mun-cod, cole-mun-name, cole-natu, cole-area
    #Add_Colegio(123, 1, 'AMAX', '456', 'myMun', 'privado', 'rural') ok
    Add_Estudiante('M', 123, 1, 'AMAX')
    Add_Estudiante('F', 234, 1, 'AMAX')
    Add_Estudiante('F', 567, 1, 'AMAX')
    res = "register-ok"

    return res

# check the global depto
@app.route("/select")
def myselect():
    lista_inc = db.session.query(Departamento.id, Departamento.departamento, Colegio.id, Colegio.dpto_cod, Colegio.dpto_nombre, Colegio.cole_area, Colegio.cole_naturaleza, Estudiante.genero, Estudiante.dpto_nombre, Estudiante.puntaje_global).\
        join(Colegio, Colegio.dpto_cod == Departamento.id).\
            join(Estudiante, Estudiante.dpto_cod == Colegio.dpto_cod).limit(3).all()
    #lista_inc = Departamento.query.all()
    print(lista_inc)
    
    #return "ok"
    if lista_inc:
        #return lista_inc
        return jsonify([{"id":r.departamento}for r in lista_inc])

# ESTE QUERY va a ser para el departamento - y que dsaque el global del estudiante
@app.route("/get")
def myget():
    if 'id' in request.args:
        estu_id = request.args['id']
    else:
        return 'id no encontrado'
    lista_get = Get_Estudiante(estu_id)
    if lista_get:
        print(lista_get)
        #return "ok get"
        return jsonify([{"id":r.id, "puntaje_global":r.puntaje_global, "genero":r.genero}for r in lista_get])






## Funciones en la DB
def CrearEsquema():
    db.create_all()


def Add_Depto(_id, _name, _global_prom):
    reg_dpto = Departamento(id=_id, departamento=_name, glob_prom=_global_prom)
    db.session.merge(reg_dpto)
    db.session.commit()
# cod-dane, dptocod, dptoname, cole-mun-cod, cole-mun-name, cole-natu, cole-area
def Add_Colegio(_cod_dane, _d_code, _d_name, _col_mun_code, _col_mun_name, _col_natu, _col_area):
    reg_est = Colegio(id = _cod_dane, dpto_cod=_d_code, dpto_nombre=_d_name, cole_mun_cod=_col_mun_code, cole_mun_nombre=_col_mun_name, cole_naturaleza=_col_natu, cole_area=_col_area)
    db.session.merge(reg_est)
    db.session.commit()

def Add_Estudiante(_id, _genero, _p_global, _dpto_cod, _dpto_nombre):
    reg_est = Estudiante(id= _id, genero=_genero, puntaje_global=_p_global, dpto_cod=_dpto_cod, dpto_nombre=_dpto_nombre)
    db.session.merge(reg_est)
    db.session.commit()

def Get_Estudiante(index):
    #result = db.session.query(Estudiante.puntaje_global).filter(Estudiante.id == index)
    result = db.session.query(Estudiante).filter_by(id = index)
    '''for row in result:
        print("Rew: ", row.fechainc)'''
    return result

def GetFields(index):
    #result = db.session.query(Estudiante.puntaje_global).filter(Estudiante.id == index)
    result = db.session.query(Estudiante)
    '''for row in result:
        print("Rew: ", row.fechainc)'''
    return result

def TotalRegisters():
    result = db.session.query(Estudiante.id).count()
    return result

def PromedioDep():
    result = db.session.query(Estudiante.id).count()
    return result



if __name__ == '__main__':
    app.run()




