# llenar la db de mongo a partir del csv
python3 CsvToMongo.py DBMongoCreation --local-scheduler

# hacer el map reduce - solo calendario B
#rm Promedio_CalB.csv
#python3 mapRD_CalendarioB.py Mapeo --local-scheduler

# hacer el map reduce
rm DptoPromedio.csv
python3 mapRD.py Mapeo --local-scheduler

# crear la db de mysql a apartir de mongo
python3 FeedMysqlDB.py DBMongoToSQL --local-scheduler

# hacer el map reduce
#rm AntioquiaPromedio.csv
#python3 mapRD.py Mapeo --local-scheduler

# correr el api
flask run
