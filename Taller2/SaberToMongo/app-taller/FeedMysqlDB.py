import sys
import csv
import os
from pymongo import * 
from sqlalchemy import inspect


import app

ip = 'localhost'
puerto = 27017
nom_db = 'Icfes'
cliente = MongoClient(ip, puerto)
db = cliente[nom_db]


import luigi


# insertar en sql database
class DBMongoToSQL(luigi.Task):
    def requires(self):
        return []

    def output(self):
        return []

    def run(self):
        app.CrearEsquema()
        # Query from app.db
        '''print('My query from sql')
        lista_get = app.Get_Estudiante(1)
        if lista_get:
            print(lista_get)
            print([{"id":r.id, "puntaje_global":r.puntaje_global, "genero":r.genero}for r in lista_get])'''

        print(' --- My query from with all data from sql --- ')
        limite = db.SaberDB.count()

        coll = db.SaberDB
        consulta = {'ESTU_COD_RESIDE_DEPTO':1}
        salida = coll.find()
        print(limite)
        lista_dpto_cod = []
        lista_colegio_codigodane = []
        lista_estudiante = []
        if salida:
            for r in salida:
                if r['COLE_COD_DEPTO_UBICACION']:
                    if r['COLE_COD_DEPTO_UBICACION'] not in lista_dpto_cod:
                        _depto = r['COLE_DEPTO_UBICACION']
                        consulta = db.DptoPromedio.find_one({"_id": _depto})
                        promedio_dpto = float(consulta['value'])

                        lista_dpto_cod.append(r['COLE_COD_DEPTO_UBICACION'])
                        app.Add_Depto(int(r['COLE_COD_DEPTO_UBICACION']), r['COLE_DEPTO_UBICACION'], promedio_dpto)
                        print("Los datos DPTO:: ", int(r['COLE_COD_DEPTO_UBICACION']), r['COLE_DEPTO_UBICACION'], promedio_dpto)
                    
                if r['COLE_COD_DANE_SEDE']:
                    if r['COLE_COD_DANE_SEDE'] not in lista_colegio_codigodane:
                        lista_colegio_codigodane.append(r['COLE_COD_DANE_SEDE'])
                        app.Add_Colegio(r['COLE_COD_DANE_SEDE'], r['COLE_COD_DEPTO_UBICACION'], r['COLE_DEPTO_UBICACION'], r['COLE_COD_MCPIO_UBICACION'], r['COLE_MCPIO_UBICACION'],r['COLE_NATURALEZA'],r['COLE_AREA_UBICACION'])
                        print("Los datos COLEGIO:: ", int(r['COLE_COD_DANE_SEDE']), r['COLE_COD_DEPTO_UBICACION'], r['COLE_DEPTO_UBICACION'], r['COLE_COD_MCPIO_UBICACION'], r['COLE_MCPIO_UBICACION'],r['COLE_NATURALEZA'],r['COLE_AREA_UBICACION'])

                '''if r['COLE_CALENDARIO']:
                    if r['COLE_CALENDARIO'] == 'B':'''
                app.Add_Estudiante(r['ESTU_CONSECUTIVO'], r['ESTU_GENERO'], int(r['PUNT_GLOBAL']), r['COLE_COD_DEPTO_UBICACION'], r['COLE_DEPTO_UBICACION'])
                print("Los datos ESTU:: ", r['ESTU_CONSECUTIVO'], r['ESTU_GENERO'], int(r['PUNT_GLOBAL']), r['COLE_COD_DEPTO_UBICACION'], r['COLE_DEPTO_UBICACION'] )

if __name__ == '__main__':
    luigi.run()