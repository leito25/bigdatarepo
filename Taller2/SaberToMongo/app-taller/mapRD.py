import sys
import csv
import os
from pymongo import * 
from sqlalchemy import inspect


import app

ip = 'localhost'
puerto = 27017
nom_db = 'Icfes'
cliente = MongoClient(ip, puerto)
db = cliente[nom_db]


import luigi



class Mapeo(luigi.Task):
    def requires(self):
        return []

    def output(self):
        return luigi.LocalTarget('DptoPromedio.csv')

    def run(self):
        # comando para ejecutar el js que tine el MapReduce
        os.system('mongosh < promAllData.js')
        # Crear la tabla resultado al DrugsDB
        os.system('mongoexport --db Icfes --collection DptoPromedio --type=csv --fields _id,value --out DptoPromedio.csv')


if __name__ == '__main__':
    luigi.run()