# alembic - alchemy
from sqlalchemy.sql.expression import null
import db
from modelost import *

def Add_Dpto(dep):
    reg_dpto = Departamento(departamento=dep)
    db.session.merge(reg_dpto) # utilizo el merge para evitar repeticiones
    db.session.commit()

def Add_Municipio(mun, cdane, dpto):
    reg_mun = Municipio(id=cdane, municipio=mun, depto=dpto)
    db.session.merge(reg_mun)
    db.session.commit()

def Add_Incautacion(fecha, claseb, cant, cdane):
    reg_inca = Incautacion(fechainc=fecha, clasebien=claseb, cantidad=cant, codigodane=cdane)
    db.session.add(reg_inca)
    db.session.commit()

def myQuery():
    # Esta query trae todo pero no me parece adecuada, ya que si se cuelga
    # se arruinaría todo el proceso
    result = db.session.query(Departamento.departamento, Municipio.municipio, Incautacion.codigodane, Incautacion.clasebien, Incautacion.fechainc, Incautacion.fechainc, Incautacion.cantidad).\
        join(Municipio, Municipio.id == Incautacion.codigodane).\
            join(Departamento, Departamento.departamento == Municipio.depto).all()

    return result

# Get registro one by one
def myQueryOne(index):
    # Query de registro en registro basado en un index
    # enviado desde luigiExtractFromSQL
    result = db.session.query(Departamento.departamento, Municipio.municipio, Incautacion.codigodane, Incautacion.clasebien, Incautacion.fechainc, Incautacion.cantidad).\
        join(Municipio, Municipio.id == Incautacion.codigodane).\
            join(Departamento, Departamento.departamento == Municipio.depto).filter(Incautacion.id == index)
    
    return result

#Número total de registros
def TotalRegisters():
    result = db.session.query(Incautacion.id).count()
    return result


# Agregue este método para limpiar las tablas y volverlas a crear
# al momento de vaciar el .cvs en la database
def Init_DB():
    db.Base.metadata.drop_all(db.motor)
    db.Base.metadata.create_all(db.motor)

# Esto solo se ejecuta cuando se corre principal.py
if __name__ == '__main__':
    db.Base.metadata.drop_all()
    db.Base.metadata.create_all(db.motor)