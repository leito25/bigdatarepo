# Borrar archivos anteriores
rm sqlDB_test.csv
rm dptos.txt
rm InfoDrugsAntioquia_test.csv

# Extracción de datos de SQL y Creación de mongo database a partir de un CSV output
# output sqlDB_test.csv / sqlIncautacionesDB.csv
python3 luigiExtractFromSQLDB.py DBMongoCreation --local-scheduler

# Consulta de cantidad de droga según el tipo
# output InfoDrugsAntioquia.csv
python3 ConsultasLM_Drugs.py ExtraerMap --local-scheduler

nano InfoDrugsAntioquia_test.csv