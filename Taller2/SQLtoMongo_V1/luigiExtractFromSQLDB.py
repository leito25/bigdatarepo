import sys
import csv
import os
from pymongo import * 
from principal import *
from sqlalchemy import inspect

ip = 'localhost'
puerto = 27017
nom_db = 'DrugsDB_test'
cliente = MongoClient(ip, puerto)
db = cliente[nom_db]


import luigi

# python3 luigiExtractFromSQLDB.py DBMongoCreation --local-scheduler
                

# Se toma el número de registros en SQL+1
# y se hace de una en una las querys y se copian al csv.
class DBExtract(luigi.Task):
    def requires(self):
        return []

    def output(self):
        #sqlDB.csv cambiar por sqlDB_test.csv for testing
        return luigi.LocalTarget('sqlDB_test.csv')

    def run(self):
        with self.output().open('w') as f_out:
            # número de registros en la DB MySQL
            limit = TotalRegisters()
            print("Número de registro en la DB MySQL: ", limit)
            # Se pone el limite como registros tope, en 10 de prueba
            for reg in range(1, 10):#limit + 1):
                myLine = myQueryOne(reg)
                for row in myLine:
                    myLineRow = str(row)
                    myLineRow = myLineRow.replace("('", "").replace("'", "").replace("("," ").replace(")","").replace(" CT", "(CT)").replace(", ",",")
                    print("OnlyOne: ", myLineRow)
                    f_out.write('{}\n'.format(myLineRow))

# AMAZONAS,LETICIA (CT),91001000,COCAINA,1/01/2010,8825
# DEPARTAMENTO,MUNICIPIO,CODIGO DANE,CLASE BIEN,FECHA HECHO,CANTIDAD
class DBMongoCreation(luigi.Task):
    def requires(self):
        return [DBExtract()]

    def output(self):
        return []

    def run(self):
        # Aquí se hacen las inserciones a Mongo
        # a partir del 'sqlDB.csv'
        with self.input()[0].open() as f_in:
            db.Incautaciones_test.drop()
            db.AntioquiaDrugs_test.drop()
            for linea in f_in:
                linea = linea.strip()
                cad = linea.split(',')
                # insertar la cantidad como número no como string
                cantField = str(cad[5])
                cantField = cantField.replace("'","")
                intCantidad = cantField
                insercion = {'DEPARTAMENTO':cad[0], 'MUNICIPIO':cad[1], 'CODIGO_DEPARTAMENTO':cad[2], 'CLASE_BIEN':cad[3], 'FECHA':cad[4], 'CANTIDAD':int(intCantidad)}
                print(insercion)
                
                #inserción a la DB DrugsDB -> Collection::Incautaciones
                registros = db.Incautaciones_test.insert([insercion])


if __name__ == '__main__':
    luigi.run()