# Reduce Map

import luigi
import os
from pymongo import * 

# python3 ConsultasLM_Drugs.py ExtraerMap --local-scheduler

ip = 'localhost'
puerto = 27017
nom_db = 'DrugsDB_test'
cliente = MongoClient(ip, puerto)
db = cliente[nom_db]

class SalidaMapeo(luigi.Task):
    def requires(self):
        return []

    def output(self):
        return luigi.LocalTarget('InfoDrugsAntioquia_test.csv')

    def run(self):
        # comando para ejecutar el js que tine el MapReduce
        os.system('mongosh < drugsAntioquia.js')
        # Crear la tabla resultado al DrugsDB
        os.system('mongoexport --db DrugsDB_test --collection AntioquiaDrugs_test --type=csv --fields _id,value --out InfoDrugsAntioquia_test.csv')


class ExtraerMap(luigi.Task):
    def requires(self):
        return[SalidaMapeo()]

    def output(self):
        return []

    def run(self):
        with self.input()[0].open() as f_in:
            print("Cantidad según tipo de droga: ")
            for linea in f_in:
                linea = linea.strip()
                cad = linea.split(',')
                reg = {cad[0]:cad[1]}
                print(reg)

if __name__ == '__main__':
    luigi.run()
    