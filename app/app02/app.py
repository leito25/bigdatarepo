from jinja2.loaders import ModuleLoader
from werkzeug.utils import import_string


import flask
from flask import Flask, jsonify, request
from flask_pymongo import PyMongo
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql.sqltypes import Float, String, Text, Integer
from sqlalchemy.sql.schema import ForeignKey


import sys
import csv
import os

from pymongo import * 


app=Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:leito123@localhost/mybigdatabase'
db = SQLAlchemy(app)

#modelo
class Departamento(db.Model):
    __tablename__='departamento'
    departamento=db.Column('ndepartamento', String(100), primary_key=True)

class Municipio(db.Base):
    __tablename__='municipio'
    id=db.Column('codigodane', String(20), primary_key = True)
    municipio=db.Column('municipio', Text)
    depto = db.Column('ndepartamento', String(100), ForeignKey(Departamento.departamento))

class Incautacion(db.Base):
    __tablename__ = 'incautacion'
    id = db.Column(Integer, primary_key = True)
    fechainc=db.Column('fecha', Text)
    clasebien=db.Column('clasebien', Text)
    cantidad=db.Column('cantidad', Integer)
    codigodane=db.Column('codigodane', String(20), ForeignKey(Municipio.id))

# SQL Init
@app.route("/")
def home():
    return "<p> Init </p>"

    # SQL Init
@app.route("/incautaciones")
def inc():
    lista_inc = Departamento.query.all()
    if lista_inc:
        return jsonify([{"ndepartamento":r.departamento}for r in lista_inc])



if __name__ == '__main__':
    app.run()