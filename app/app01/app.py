from werkzeug.utils import import_string
import flask
from flask import Flask, jsonify, request
from flask_pymongo import PyMongo

import sys
import csv
import os

from pymongo import * 

ip = 'localhost'
puerto = 27017
nom_db = 'DrugsDB_test'
cliente = MongoClient(ip, puerto)
db = cliente[nom_db]

app = Flask(__name__)
app.config['MONGO_DBNAME'] = 'DrugsDB_test'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/DrugsDB_test'
mongo = PyMongo(app)

# dict de libros
libros=[{'id':0, 'titulo':'Nostromo'}, {'id':1, 'titulo':'Código Da Vinci'}]


# decorators

# cuando este en la raiz
@app.route("/")
def home():
    return "<p> Home </p>"

# ruta inicio
@app.route("/inicio")
def inicio():
    return jsonify(libros)

# ruta inicio
@app.route("/consulta", methods = ["GET"])
def consulta():
    if 'id' in request.args:
        id = int(request.args['id'])
    else:
        return 'id no encontrado'

    for l in libros:
        if l['id'] == id:
            return jsonify(l)

@app.route("/mongo", methods = ['GET'])
def consultaMongo():
    if 'dep' in request.args:
        dpto = request.args['dep']
    else:
        return 'dpto no encontrado'

    '''sv = mongo.db.Incautaciones_test
    s = sv.find_one({'DEPARTAMENTO':dpto})
    if s:
        salida = {'DEPARTAMENTO':s['DEPARTAMENTO'],'MUNICIPIO':s['MUNICIPIO']}
    else:
        salida = 'No encontrado'
'''

    sv = mongo.db.Incautaciones_test
    consulta = {'DEPARTAMENTO':dpto}
    salida = sv.find(consulta).limit(5)
    '''s = sv.find_one({'DEPARTAMENTO':dpto})
    if s:
        salida = {'DEPARTAMENTO':s['DEPARTAMENTO'],'MUNICIPIO':s['MUNICIPIO']}
    else:
        salida = 'No encontrado

    return  jsonify({'resultado':salida})'''
    if salida:
        return jsonify([{'DEPARTAMENTO':r['DEPARTAMENTO'],'MUNICIPIO':r['MUNICIPIO']}for r in salida])

@app.route("/doble", methods = ['GET'])
def consultaDobleArg():
    if 'dep' in request.args:
        dpto = request.args['dep']
    else:
        return 'dpto no encontrado'

    sv = mongo.db.Incautaciones_test
    consulta = {'DEPARTAMENTO':dpto}
    #s = sv.find(consulta.limit(5))
    s = sv.find({'DEPARTAMENTO':dpto})
    if s:
        salida = {'DEPARTAMENTO':s['DEPARTAMENTO'],'MUNICIPIO':s['MUNICIPIO']}
    else:
        salida = 'No encontrado'

    return  jsonify({'resultado':salida})


# http://127.0.0.1:5000/mongo?dep=ANTIOQUIA
# http://127.0.0.1:5000/mongo?dep=ANTIOQUIA&mun=ANTIOQUIA





if __name__ == '__main__':
    app.run(debug=True, port=30505)



'''app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello, World!"'''