import streamlit as st
import pandas as pd
import numpy as np

import sys
import csv
import os
from pymongo import * 
from sqlalchemy import inspect

st.title('Barra Lateral')

ip = 'localhost'
puerto = 27017
nom_db = 'Icfes'
cliente = MongoClient(ip, puerto)
db = cliente[nom_db]


dep = db.DptoPromedio.find()
mensaje = ""
dpto = [d['_id'] for d in dep]

def prom(_sel):
    #solo un campo
    myAnswer = db.DptoPromedio.find_one({"_id": _sel})
    return myAnswer['value']

    #todo
    '''myDep = db.DptoPromedio.find()
    for d in myDep:
        if d['_id'] == _sel:
            return d['value']'''
            
seleccion = st.sidebar.selectbox('Seleccionar si o no:', dpto)
valor = st.sidebar.slider('valor') # limite para una tabla

st.write(f'Has Seleccionado el departamento de {seleccion} y este es el promedio: {prom(seleccion)}')
